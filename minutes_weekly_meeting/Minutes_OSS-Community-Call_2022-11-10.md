# Meeting notes Gaia-X OSS community November 10, 2022

## Focus of the weekly

   * Events
   * New Pontus-X Gaia-X Web3 Ecosystem and GEN-X network


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. AOB



## General Notes

**Participants in the call:** [...]

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to the minutes:** [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting)

[https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-11-03.md](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-11-03.md)



### Introduction of new participants and regular contributors

   * Antti Poikola, SITRA, Gaia-X Hub Finland, [https://www.gaiax.fi/](https://www.gaiax.fi/)


**Useful resources for newcomers:** The Gaia-X Framework. [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)



### Interesting sessions, events and news

**We're also collecting all upcoming events here:** [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)



   * **Gaia-X Summit 2022**
       * Nov 17 - 18, Online Attendance (unlimited), Physical Attendance (very limited)
       * Link: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)


   * **GXFS Roadshow**
       * [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **Gaia-X Hub Germany Assembly**
       * Friday, November 11
       * Gaia-X Perspectives and Lighthouse Projects with MDS, Gaia-X 4 Future Mobility, EuProGigant and Agri-Gaia
       * Peter Kraemer, Jan Fischer und Anna-Raphaela Schmitz


   * **European Big Data Forum**
       * Nov 21 - 23, Prague (CZ)
       * Link: [https://european-big-data-value-forum.eu/2022-edition/programme/](https://european-big-data-value-forum.eu/2022-edition/programme/)


   * **GC Tech-Talk No. 6**
       * „Secure Operating System and Test Automation - lessons learned and future perspectives“ (Tor Lund Larsen, Cyberus Technology)
       * Nov 24, 2 - 3 pm, online
       * Link: [https://mautic.cloudandheat.com/techtalk-registration](https://mautic.cloudandheat.com/techtalk-registration)


   * **Gaia-X Roadshow powered by eco**
       * Nov 30 in Frankfurt, date for Hamburg will follow soon, Viena-Austria 16.03.2022, 
       * Link: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **Event by Gaia-X Finland** 
       * "Gaia-X Finland: Data Sharing for Breakfast" and "Data Spaces Technology Landscape 2023"
       * December 14, Helsinki (FIN)
       * Link: [https://www.gaiax.fi/](https://www.gaiax.fi/)
       * [https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event](https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event)


   * **Mobility Meetup with the Gaia-X Hub Netherlands and the Dutch Blockchain Coalition**
       * November 24
       * [https://www.dutchblockchaincoalition.org/agenda/mobility-meetup-lancering-future-mobility-data-marketplace](https://www.dutchblockchaincoalition.org/agenda/mobility-meetup-lancering-future-mobility-data-marketplace)


### AOB

   * **Web3 Ecosystem Relaunch with GEN-X network**
       * Pontus-X - Gaia-X Web3 Ecosystem Portal Migrated [https://portal.minimal-gaia-x.eu/](https://portal.minimal-gaia-x.eu/)
       * EuProGigant migrated - [https://euprogigant.portal.minimal-gaia-x.eu/](https://euprogigant.portal.minimal-gaia-x.eu/)
       * moveID migrated - [https://portal.moveid.eu/](https://portal.moveid.eu/)
       * SBB Open Science Portal migrated - [https://sbb.portal.minimal-gaia-x.eu/](https://sbb.portal.minimal-gaia-x.eu/)
       * FMDM migrated - [https://marketplace.future-mobility-alliance.org/](https://marketplace.future-mobility-alliance.org/)
       * Logging Service migrated - [https://logging.genx.minimal-gaia-x.eu/](https://logging.genx.minimal-gaia-x.eu/)
   * New network built in a strategic partnership with Polygon / Polygon Edge / Polygon ID
   * Gaia-X Self-Descriptions mandatory for all participants
   * Building towards the Federation Services v2 specifications and Trust Framework / Compliance Service Deployment Scenarios
   * Transparent Governance with all members eligible for voting 
   * Network will be federated across Europe with Federators willing to commit to a sustainable network and the Gaia-X Trust Framework
   * Self-Descriptions for the Infrastructure and Participants to be published together in line with the SD Working Group