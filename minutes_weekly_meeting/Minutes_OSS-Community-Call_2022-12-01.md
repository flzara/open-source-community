# Meeting notes Gaia-X OSS community December 1, 2022

## Focus of the weekly

   * Hackathon No. 6 - first steps of planning the event


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Hackathon No. 6

6. AOB



## General Notes

**Participants in the call:** 19

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-11-24.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-11-24.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Viridiana Alverde


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)


### Interesting sessions, events and news

**We're also collecting all upcoming events here:** [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)



   * **How to subscribe to the Gaia-X Tech Newsletter**
       * Link: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


   * **Video for the technical Demo during the Gaia-X Summit 2022 now available**
       * Link: [https://www.youtube.com/watch?v=JX9OOl7jVD8](https://www.youtube.com/watch?v=JX9OOl7jVD8) 


   * **Service Composition Workshops took place**
       * Link: [https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/packages/10983186](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/packages/10983186)
       * Documentation \& results: Kai will follow up on where to find all the latest results in "one place"


   * **GXFS Roadshow Frankfurt**
       * Nov 30
       * Took place


   * **Data Spaces Support Centre**
       * Dec 1, 16:00 - 17:00 CET [https://zoom.us/webinar/register/WN\_OyD33OngTjWC3S2TpeNpPw](https://zoom.us/webinar/register/WN\_OyD33OngTjWC3S2TpeNpPw)
       * Link: [https://dssc.eu/starter-kit/](https://dssc.eu/starter-kit/)


   * **Open Source Workshops for Computing \& Sustainability**
       * Dec 2, Brussels (BEL)
       * Location: At Albert Borschette Congress Center (CCAB)
       * Organised: By the European Commission in collaboration with the SWForum.eu Coordination and Support Action
       * Link: [https://www.swforum.eu/events/open-source-workshops-computing-sustainability](https://www.swforum.eu/events/open-source-workshops-computing-sustainability)


   * **Event by Gaia-X Finland** 
       * "Gaia-X Finland: Data Sharing for Breakfast" and "Data Spaces Technology Landscape 2023"
       * Dec 14, Helsinki (FIN)
       * Link: [https://www.gaiax.fi/](https://www.gaiax.fi/)
       * [https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event](https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event)


   * **Lightning Talk Gaia-X Federation Services on Sovereign Cloud Stack** 
       * Next Thursday, 2022-12-08 at 15:45 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)


   * **Deepdive Gaia-X Federation Services on Sovereign Cloud Stack**
       * 2023-01-16 at 15:05 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)

### Hackathon No. 6

 **Discussion around the date**

   * Proposal for finding a fitting week: Mar 20-24 or Mar 27-31 (will not be a full week!)
       * First impression: Mar 20-24 seems to be preferred, needs to be aligned with Gaia-X events and roadmap.
           * Lets not communicate this as a fixed data, but a proposal for now!
           * Potential collision: CloudFest on Mar 21-23


**Location**

   * Needs to be figured out!
       * We were in France and Germany - what's next?
       * Enrique Areizaga Sanchez: If you want to come to Spain, Tecnalia volunteers to organise!


**New format idea: Discussing technical challenges**

   * More "brainstorming"/workshop sessions rather than only hacking/putting possible solutions into action during the Hackathon
   * Will not replace the hacking! Will be an addition!
   * Question to the audience:Would it make sense to have some kind of discussion meetings?
       * Feedback: Hackathon will probably be onsite, more dynamic, more action - should not be too much talking
       * How many people will really want to do both? Hacking and brainstorming workshop?
           * Option 1: Do it in parallel
           * Option 2: Extend Hackathon to three days and have one day for brainstorming/workshop and two days for hacking
               * Seems to be the preferred option for now!
               * Idea: Make it a hybrid event so that people can join online for the workshop day and attend in person for the hacking (three days attendance could be to expensive for some companies/organizations)


**When will we start planning?**

   * We need to reach out to the lighthouse projects and verticals early enough!
       * At least 3 months ahead of schedule
       * Let's start with planning in 2022 already to bring everyone together for the Hackathon.
           * Collecting track ideas etc


### AOB



   * The Clearing Houses will be several from centralized to decentralized versions that are being worked on today [https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/](https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/) 
   * We want to avoid bottlenecks and single points of failures, but we need to start somewhere, 3 for the beginning
   * If you want to get started with your Self-Descriptions as Institution please let us (deltaDAO, Gaia-X Web3 Ecosystem participants) know. We are onboarding and helping all the community members along as we believe we should have valid self-descriptions for all community members in the next months to bring the identity game to the next level and to build on that. 
