MVG sessions 31-03-2022

1. Interesting sessions/events/news 

- https://www.luxinnovation.lu/event/gaia-x-eus-most-important-aspiration-in-a-generation-challenges-first-achievements/

Kurt Garloff (via Chat): 
- The only OWP remaining obviously is interesting to the community. Great!

2. Gaia-X Hackathon #3 - recap 

Kai Meinke: 
- Main goal was to integrate the Trust Framework into the minimal viable Gaia-X and to include the Identity Layer
- It’s working -> you can issue and register credentials 
- You’re now able to use your credentials to verify you’re identity as onboarding participants using open-Id connect 
- Each participants can check the status of the service provider and federators
- Who wants to become a federator can do that on every cloud deck using Kubernetes 
- The components are open-source and available 
- A lot of people requested it but there weren’t able to enter (no GitLab) -> ongoing discussion how to open it 

Kurt Garloff: 
- If I count the people who wanted to discuss but not contribute to code there was a ratio of 10:1 
- Maybe it was a mistake of our track and wrong advertisement or did every track have this ratio?

Kai Meinke:
- I can confirm that
- We provided details about how to prepare but we can improve the process of onboarding and registering
- Maybe having dedicated setup session or a week before the hackathon to attract developers
- Maybe Bounties for most active contributer

Marius Feldmann: 
- Would you say that the number increase in comparison of the last two hackathons or is it stable?

Kurt Garloff: 
- The statistics of the people contributing outside of the people we personally recruited was limited 

Kai Meinke: 
- Before the hackathon we saw a lot of people collaborating across the different groups 
- For the most Newcomer it was the first hackathon -> not easy to onboard them
- The onboarding funnel for developers can be improved (for developers at least)

Martin Pilka: 
- We haven’t seen only few contributors
- I agree, it’s not easy to onboard them

Matej Feder: 
- Attendees were more confident 
- Bounties -> Promote the hackathon goals external -> Motivate the external teams to create their own presentations 

Martin Pilka: 
- Focus on less coding goals -> because not many active contributors 
- Less and more focus coding goals attracts more contributors
- Maybe 1 or 2 well documented and prepared coding goals 

Kai Meinke: 
- We could have a "preparation hackathon" to get prepares
- If it’s probably incentivized they would spend extra time for this 
- I saw other qualification hackathons with small bounties 
- At the moment it takes low effort to qualify for the hackathon -> because we want to keep it open -> but wide range of agenda and not overall good prepared
- But if we keep the entry requirements higher -> more structure and preparation 
- Maybe we have less slots but more time for hacking 

Martin Pilka: 
- Maybe we have too much content, its even for me difficult to follow -> presentation up to 300 slides 

Kai Meinke: 
- It becomes easier for the next hackathons because of the documents -> repeatable 

Marius Feldmann: 
- The problem is there are partially activities in the Gaia-X context that are doing separat stuff (e.g. 11 research projects)
- There is a overall synchronisation but not code level -> potential fo working together and bringing people together during the hackathon
- It’s important to identify which synergies exist technology-wise among existing activities (e.g. common technologies)
- We should positionate the Hackathon in a way to synergise these activities
- Keep in mind bounties are not everything -> for example decision between working on a customer project or getting some bounties 

Pierre Gronlier: 
- There will be a new Program Management Team with 3 people
- They are all have their expertise and can advertise/support all streamlines (e.g. one is more open-source-oriented)
- With this change the next hackathon will more aligned with that what you said before 

Marius Feldmann:
- To sum up, the goal is to motivating people to contribute technical wise 
- All in all the situation was stable with a typical ratio but we should definitely work on activating more people
- The increasing number of people can mean also people who contribute technical wise
- We should work to advertise the event towards further companies

Kai Meinke: 
- We should give open-source projects more visibility if they are active in the Gaia-X community and stack
- There are some of those projects  where it is not yet visible enough how important they are for the Gaia-X community 
- Rather an full and content-ready event page than the GitLab page
- There’s improvement regarding the Onboarding-Funnel 

Pierre Gronlier:
- Maybe we can make a public event page and keep the rest of the website private
- But the wiki page should be not for everyone 

Pierre Gronlier: 
- We should keep two days because extending will be difficult -> but less Tracks

Marius Feldmann: 
- Or two full-blown days and the third day for presentation

Kai Meinke: 
- Learning: This time it was valuable that we frontloaded the first day, to visit other Tracks on the second day (able to contribute more freely)

Kurt Garloff: 
- I like the proposal from Pierre -> more focus in the hackathon

Marius Feldmann: 
- Extension proposal: Having a intro about how the content is relevant in the global Gaia-X context for a more focus hackathon 
- with also 2-3 technical talks
- Keep in mind: There are not a lot real technical wise community events in Gaia-X

Pierre Gronlier: 
- Maybe a technical communication channel 

Marius Feldmann: 
- Would you all agree with:  -> Kick-Start with an overall global picture where we are -> Focus presentation in the beginning   -> Then more single focus for the hackathon -> Not a lot of separate tracks 

Pierre Gronlier: 
- The topics should meet the Hackathons’ overall mission

Marius Feldmann: 
- It is important to have a full alignment and have some political insights but not block the activities we have here

[Via Chat]

Kurt Garloff (via Chat):
- The preparation Hackathon is the real Hackathon then.
- Should we try to do a hack week next time?
- Less parallel tracks, prep sessions that fit in the schedule ...

Marius Shekow (via Chat):
- Have all slides already been collected and uploaded to the Hackathon3 Wiki page? I noticed, for instance, that the Track 3 presentation for the session "Status Quo GXFS-DE project and exchange" seems to be missing. I particularly mean the session where Lauresha explained how a company can apply as an organization to join a federation.

Lauresha Memeti (via Chat): 
- The slides are shared with the team, I assume they will be uploaded soon .. If you would like to have them faster please drop me an e-mail and I would gladly share them with you 

Martin Pilka (via Chat): 
- @Marius: We are still collecting them and compiling final presentation, will be sent to mailing list by tomorrow EOB.
- I just uploaded Track 3 (GXFS) PPT/PDF
https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/wikis/GX-Hackathon-3#presentations-used-during-the-hackathon

Patrick Masse (via Chat): 
- Kai Meinke's point related to Gaia-x / pre-existing open source projects is important. This will help attract new contributors
- Could a package of presentations / videos / etc for new-comers be consolidated and used as pre-requisites for new comers before joining the Hackathon ? This would help to have a common understanding about Gaia-x while contributing to the Hackathon

Joseph Blanch (via Chat): 
- I am against reducing the scope. I am in the same situation as Kurt, that I would have liked to join more tracks but it was not possible because of schedule
- Registering all sessions, at least the non coding ones, would be great to help disseminate off-line

Matej Feder (via Chat): 
- From the docs: Aries protocols include, most importantly, protocols for issuing, verifying, and holding verifiable credentials using both Hyperledger Indy AnonCreds verifiable credential format, and the W3C Standard Verifiable Credential format using JSON-LD.


3. MVG group next steps / focus topics 

Marius Feldmann:
- What is the focus for the next 3-6 months?

Pierre Gronlier:
- 1st topic: Prototyping
- Focusing on opening connect
- We have to figure out the common vocabulary for the type/cluster of the services 
- With the credentials we can carry along to the wallets
- 2nd topic: data exchange (data protocol and format…)
- Control the access of data

Marius Feldmann:
- What it’s important is that we embedded an overall story (e.g. Pilot-005 story)
- > So that we have a complete scenario to have an end-to-end test
- > So that we not have a fragmented setup
- If we focus on data what role does the infrastructure does play here?

Pierre Gronlier: 
- I see some paper mentioning data ecosystems and infrastructure ecosystems
- Knowing where is your data or where your data is processed by who  -> part of the control you need and to control the level of autonomy 

Marius Feldmann: 
- Politics -> something we should point out in the technical discussions we have

Pierre Gronlier: 
- We need to have software to process the data -> not good if you don’t know who is owning your software
- Private company in Ukraine situation -> License Export Control -> Every company has the rights to change their license
- Software and infrastructure is as important as knowing about the data

Amjad Ibrahim: 
-  Regarding future credentials bridge -> we already have one contribution during the hackathon in track 2 

Via Chat: 
Jospeh Blanch: 
- When sharing data a key element is sovereignty, which I am not sure all dataspaces have
- In My opinion it is about knowing the legislation on the operation of the ecosystem

Kurt Garloff: 
- We have published a whitepaper on the cloud report that does define 4 aspects of sovereignty ...
- (1) Compliance, GDPR
(2) Choice (which requires compatibilty/interoperability to be practical)
(3) Technology transparency, ability to adjust, contribute, shape (OSS)
(4) Skills and Transparency on operations (Open Ops)

Matej Feder: 
- Regarding Hyperledger Aries topic: From the docs: Aries protocols include, most importantly, protocols for issuing, verifying, and holding verifiable credentials using both Hyperledger Indy AnonCreds verifiable credential format, and the W3C Standard Verifiable Credential format using JSON-LD.

4. AOB 
[none]
