# **Meeting notes Gaia-X OSS community August 18, 2022**



## Focus of the weekly

- Mission document
- Hackathon No. 5
- Onboarding Guide



## Proposed agenda



   * Competition \& Antitrust Guidelines
   * Acceptance of Agenda
   * Introduction of new participants and regular contributors 
   * Interesting sessions, events and news
   * Organizational topics
       * Mailing list during vacation times
   * Work on the deliverables
       * Mission document
       * Organization of the Gaia-X Hackathon
       * Onboarding Guide to Gaia-X Software and Ecosystem
       * Demonstrations
       * Release of Gaia-X Open-Source Software
   * AOB


## General notes

- Meeting notes from last week: [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022\_08-11.md](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022\_08-11.md)



- **Participants in today's call:** 15



- **Acceptance of Agenda:** accepted, no additional topics added.



### **Introduction of new participants and regular contributors:**



   * Roberto Garcia ([https://rhizomik.net/~roberto)](https://rhizomik.net/~roberto))


### Sessions, events and news

   * GXFS Connect [https://www.gxfs.eu/de/gxfs-connect-2022/](https://www.gxfs.eu/de/gxfs-connect-2022/)
   * Agriculture Data Space Event: [https://gaia-x.eu/event/agriculture-data-space-event/](https://gaia-x.eu/event/agriculture-data-space-event/)
   * Austrian Gaia-X Hub: Opening session (link not available)


### Organizational topics

   * Vacation Time
* Approvals for mailing list will take a bit longer than usual


### **Mission Document**

   * MR waiting for approval
   * [https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/merge\_requests/153](https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/merge\_requests/153)
   * Added some changes after TC discussion last week
       * More specific on "Out of scope" (*inclusion of Gaia-X framework* -> TC and Gaia-X CTO) and "Input" (*All OSS contributions are subject to the Gaia-X Free and Open-Source policy and the Gaia-X Intellectual Property Rules*)


### Hackathon

   * Link: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5)
   * Scenarios, goals and GXFS-DE implementation updated
       * Real-life end-to-end scenarios
   * First proposal submitted: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals)
       * 01 MoveID - Integration of a Mobility Data Ecosystem into the Gaia-X Framework
       * At least 6 proposals expected from GXFS (Lauresha)
   * **Keep in mind:** You do not have to *own* the proposal, just add if you think something could be interesting!
       * Template for proposals available 


### Onboarding Guide

   * Links: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Onboarding-Guide](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Onboarding-Guide)
   * Proposals to add videos and framework diagram ([https://docs.gaia-x.eu/framework/)](https://docs.gaia-x.eu/framework/))
   * Lets add and onboarding Box as well and bring this together.
       * Adding the Federation Service Deep Dives
       * Additional resources you'd like to see added?
       * Questions you'd like to see answered?





