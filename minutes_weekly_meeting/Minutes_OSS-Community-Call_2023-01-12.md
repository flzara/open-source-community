# Meeting notes Gaia-X OSS community January 12, 2023

## Focus of the weekly

   * Hackathon No. 6
   * Welcome to 2023


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda - accepted

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Hackathon No. 6

6. AOB



## General Notes

**Participants in the call:** not noted.

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-12-15.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-12-15.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Florent Zara, Eclipse Foundation, Onboarding the OSS projects, as the GXFS-DE move over to Eclipse and the process has begun to make the transition.
   * Ryan Ford, VTT, Research Scientist 
   * Arian Firouzbakhsh, Data Space Architect IONOS, 
   * Veronika Siska, AIT Austrian Institute of Technology, 
   * Vivien Witt, eco / GXFS-DE
   * Wolfgang Thronicke, ATOS, moveID 
   * Angel Palomares Perez, ATOS


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news



   * **Lightning Talk Gaia-X Federation Services on Sovereign Cloud Stack**
       * Thursday, 2023-01-12 at 15:45 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)


   * **Deepdive Gaia-X Federation Services on Sovereign Cloud Stack**
       * Monday, 2023-01-16 at 15:05 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)


   * **Gaia-X Hub Germany - Participant Onboarding and Self-Description Workshop**
       * February, 3, 2023 09:00 to 12:00 CET
       * Organized by acatech and deltaDAO
       * Creation of Gaia-X compliant participant Self-Descriptions to reach the 50% adoption goal of 2023 and to prepare projects for the Gaia-X Clearing Houses and work with Self-Descriptions
       * Includes Onboarding for the Gaia-X Web3 ecosystem


   * **Gaia-X Roadshow Hamburg**
       * February 9, 2023


   * **Market-X, Vienna**
       * March 14-15, Vienna, location and agenda to be confirmed
       * [https://gaia-x.eu/event/market-x/](https://gaia-x.eu/event/market-x/)
       * Market-X will bring together and connect industries and technology partners as well as showcase funding opportunities through the Gaia-X Hubs.
       * Final name will be decided and more information forthcoming about next week, newsletter and brochure


   * **Data Spaces Discovery Days Netherlands - Business value of sovereign data sharing**
       * 21–23 March 2023
       * Pre-registration link: [https://internationaldataspaces.org/data-spaces-discovery-days-netherlands](https://internationaldataspaces.org/data-spaces-discovery-days-netherlands) 

### The beta version of the Gaia-X Framework

The work-in progress version of the new landscape view: [https://docs.gaia-x.eu/framework/beta/](https://docs.gaia-x.eu/framework/beta/)



### Hackathon No. 6

   * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


### AOB

   * Nothing.











