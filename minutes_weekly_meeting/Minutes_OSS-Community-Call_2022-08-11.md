**Meeting notes Gaia-X OSS community August 11, 2022**

Participants: 19

**Focus for today's weekly:**

   * Today we’ll have the demonstration of the Trust Framework 22.06. Release for about 20 minutes inkl. a short Q\&A afterwards. Repo: [https://gitlab.com/gaia-x/lab/compliance/gx-compliance](https://gitlab.com/gaia-x/lab/compliance/gx-compliance)
   * The Gaia-X Hackathon #5 has been announced and the marketing and communication materials are now available for the OSS community, please see [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/tree/main/Marketing%20and%20Communications](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/tree/main/Marketing%20and%20Communications)
   * Mission document is still pending, see [https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/merge\_requests/153](https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/merge\_requests/153)
   * We switch to the new mailing list oss-community@list.gaia-x.eu to which you can sign on through oss-community-request@list.gaia-x.eu. Please do not use the old mailing list after our next meeting.
   * Best use [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/) for signing up to the mailing list.
 

The proposed agenda for this meeting is as following:

 

   * **Competition \& Antitrust Guidelines**
       * No price fixing
       * No market or customer allocation
       * No output restrictions
       * No agreement on or exchange of competitively sensitive business information
       * Consequences of competition law infringement


   * **Acceptance of Agenda**
       * Accepted, nothing added.


   * **Introduction of new participants and regular contributors**
       * Amjad Ibrahim (Huawei)
       * Arnaud Braud
       * Christian Prehofer
       * Mario Drobics
       * Gernot Boege


       * To those on this list: Feel free to add additional information about yourself (e.g., your company, what you do in Gaia-X,...)!


   * **Interesting sessions, events and news**
       * Please add any interesting sessions, events or news (or whatever might be interesting for this group) here!
       - GXFS OCM \& PCM Deep Dive last Tuesday, August 9
       * Demonstration of the Trust Framework 22.06 Release



   * **Organizational topics**
       * Communication channels
           * New mailing list: oss-community@list.gaia-x.eu
           * Easiest way to join: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/)


   * **Work on the deliverables**
       * **Mission Document (MR153)**
           * gaia-x/technical-committee/operational-handbook!153
           * Still an open merge request
               * So far only approved by Pierre
               * If you can approve merge requests, please do so!
                   * Currently a lot of mission documents are under merge request
                   * Cristina has it on her list


       * **Organization of the Gaia-X Hackathon**

            -  Hackathon will take place September 26-27, 2022 as virtual/hybrid event (hybrid tbd).

            - We are actively looking for volunteers and session proposals for the Hackathon now. The session proposals shall relate to the goals, Gaia-X Trust Framework and Scenarios. [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home)

            - If you have ideas for session proposals please do not hesitate to add them early to [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals)

            - Gaia-X AISBL has already announced the event and we are grateful that marketing materials for sharing are now available here: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/tree/main/Marketing%20and%20Communications](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/tree/main/Marketing%20and%20Communications)

            - **Please share the news and get your network and other potential contributors involved. Thank you.**



            * No remarks from the audience



* **Onboarding Guide to Gaia-X Software and Ecosystem**

    [skipped due to lack of time]


* **Demonstrations** 

    [see sessions, events, news]

* **Release of Gaia-X Open-Source Software**

    [skipped due to lack of time]

   * **AOB**     

    [skipped due to lack of time]




**Notes:**

    

- Meeting notes will be taken collaboratively with Etherpad. Notes will stay for six days and migrate to GitLab after each weekly meeting.


**Demonstration of the 22.06 Trust Framework by Albert Peci and Moritz Kirstein, Gaia-X Lab**

- Compliance Services Repositories: [https://gitlab.com/gaia-x/lab/compliance](https://gitlab.com/gaia-x/lab/compliance)

- Please use the issues in GitLab for public communication and assistance to everybody interested and contributing can pitch in and participate.

- Swagger UI for the compliance service can be accessed here: [https://compliance.gaia-x.eu/docs/](https://compliance.gaia-x.eu/docs/)

- Swagger UI for the registry service can be found here: [https://registry.gaia-x.eu/docs/](https://registry.gaia-x.eu/docs/)

- Trust Framework can be found here: [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)

- List of Trust Anchors (currently DV SSL certificates are accepted): [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/trust\_anchors/](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/trust\_anchors/)

- deltaDAO Self-Description Signer available here: [https://github.com/deltaDAO/self-description-signer](https://github.com/deltaDAO/self-description-signer)    

- If you want to generate SDs for OpenStacked based IaaS: [https://github.com/SovereignCloudStack/gx-self-description-generator](https://github.com/SovereignCloudStack/gx-self-description-generator). Has been developed and used by SCS during the last hackathon already.

- Albert explains the full process from SD creation, DID generation, how to canonize the Self-Description and how to verify it with the compliance service and get it signed with the compliance service.

- Three main checks are performed:

    - Shape validation
    - Signature validation
    - Content validation

    - New content can be found in the changelog: [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/changelog/](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/changelog/)

    - Don't forget that the service self-description check includes a check of the participant self-description check of the service provider. A service self-description becomes invalidated if there is no Gaia-X compliant service provider associated with it.



    - Types are not needed anymore in the self-descriptions. Please note that Gaia-X terms and conditions are now mandatory.



    - Gaia-X Terms and Conditions are hashed against the text of the T\&Cs on GitLab: [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/) 



    - Note that the 22.06. version has a different way to add terms and conditions for partcipants and service offerings. participants need just the hash, service offering needs an url and a hash.



    - Please add feedback and input to the PRC and Trust Framework here: [https://gitlab.com/gaia-x/policy-rules-committee/trust-framework/](https://gitlab.com/gaia-x/policy-rules-committee/trust-framework/)



**The Gaia-X Registry**

- Gaia-X Terms and Conditions and reference shapes can be retrieved here: [https://registry.gaia-x.eu/docs/](https://registry.gaia-x.eu/docs/)
    

- To engage with the lab team (raise issues, communicate with other participants regarding development topics, etc.), use this link [https://gitlab.com/groups/gaia-x/lab/compliance/-/issues](https://gitlab.com/groups/gaia-x/lab/compliance/-/issues)





**Gaia-X OSS community mission document**

- MR 153 still open and Cristina will follow-up on it in the TC meetings. [https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/merge\_requests/153](https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/merge\_requests/153) Thank you.



