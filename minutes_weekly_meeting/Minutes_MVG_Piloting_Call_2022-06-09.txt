MVG/Piloting Call: 2022-06-09

Marius Feldmann is absent, Kai Meinke is doing the call.

1. Interesting sessions/events/news

[nothing]

2. Hackathon #4 - schedule and last preparation steps (Kai Meinke)

Registrations:
- Current registrations: 55 people
-> We need to share a lot more, spread the word
-> Registration for Gaia-X Hackathon #4 (June 20-21, 2022): https://gaia-x.eu/event/hackathon-4/

Proposals:
- Did our best to have a straight path to follow for all participants
-> All proposals that met the criteria are now on the page
-> Please note that if you want to change slots with another team (because of availiablity or because it might make sense content-wise), let us know
-> Gaia-X Hackathon #4 Agenda: https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-4/-/wikis/GX-Hackathon-4

Onboarding sessions:
- Onboarding sessions for each session (25 minutes)
-> Is meant as a quick teaser
-> Laying out what is going to happen and what the foundation is to onboard users and developers
-> This approach is a bit different than last time but this way participants can get an insight and decide where they want to go next

Hacking rooms:
- Hacking room links are added so that you can join as a developer for collaborating and coding
-> This time: More room for hacking
-> Not so many distractions, not so much going on in parallel presentation-wise

Presentation of Hackathon results:
- Depending on the results, the slot "presentation of Hackathon results" could be extended

Geert Machtelinckx on hacking room links:
- Kai Meinke: It will be individual links for each session
-> Comment Geert Machtelinckx: Idea was also to align those groups who work on similar topics (e.g., SD)
-> Kai Meinke: We will have track organizer discussion tomorrow where we will see where we have alignement so that we can put them together in Hacking rooms
--> Hacking room #1, #2..., so that some are joint

Kai Meinke on proposals:
- Proposals are linked in table 
- Would like to encourage all proposals to work on their descriptions and to add repositories and additional details as they come up during the presentation
- Please also add presentations if you already have created them, so that participants can also dive in into more detail
- Every one is invited to use the mailing list to contribute, to share, to already link interested people to their sessions
- Updating documentation before the Hackathon is also crucial for all proposals

3. AOB

Kai Meinke: 
- Hope we will get a new background, still using the old one
- Landing page will be updated, so that it is easy to navigate from there

Implementation of the Compliance Service (Kai Meinke)

- Everything including documentation and source code here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance

- Two ways to use, via Swagger or API, API is recommended
- What you can do with the Compliance Service is:
-> You can check your SD for Gaia-X Compliance, for example participant SD
-> It needs to meet the criteria of the Trust Framework AND it has to signed by you as a participant (e.g., RSA-key)
-> What you will get back is Compliance Credential
-> Gaia-X Compliance Service will add proof to your SD (also via key)
-> That can then be added to the Compliance Service again; the service will confirm that everything is valid

[further demonstration + short Q&A]