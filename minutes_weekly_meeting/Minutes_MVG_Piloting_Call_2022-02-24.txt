MMVG/Piloting Call: 2022-02-24


1. Interesting sessions/events/news

Kai Meinke: 
- 24.02.22: Event hosted by the German Federal Ministry
-> where the Minimal Viable Gaia-X Federation Services etc. will be presented

Pierre Gronlier: 
- 18.02.22: ETP4HPC Webinar - Gaia-X vision, implementation and the importance of HPC
-> The Trust Framework was presented in this webinar -> https://github.com/ticapix/chrome_ext-gum_videofx
-> Recording of the webinar:  https://www.youtube.com/watch?v=ynTG26RsADk&t=430s

Christoph Lange: 
- 15.03.22: „Guidelines towards fair data economy“ with keynotes, panels, demos, deep dives, workshops
-> Event will feature Gaia-X and the Gaia-X Federation Services will be presented 
-> The project „FAIR Data spaces“ will have several presentations 
-> Please note: Event is in german language
-> Link to the event: https://www.iuk.fraunhofer.de/de/events/fair-data.html
-> via https://mobile.twitter.com/NFDI_de/status/1496488415688331268


2. Gaia-X Trust Framework / Gaia-X compliance (Pierre Gronlier)
Link to presentation: https://community.gaia-x.eu/index.php/f/14308726

Marius Feldmann: 
- The compliance topic and the Trust Framework will be one of the core points during the hackathon and other further activities 

Pierre Gronlier: 

[Insights und current state of Compliance and Trust Framework]
- Current state of the document: Draft where requirements from the DSBC position paper and the architecture document were extracted  
-> While compiling this together, the Trust Framework document was created 
-> Link to this document: https://gaia-x.atlassian.net/browse/LAB-88
- Compliance working group: Kick-off yesterday -> started to write the initial document and there is still work to do
-> The Gaia-X Ecosystem is the virtual setup of participants and service offering that fulfills the requirement of this document 

[Short presentation of the framework and why are we doing it]
- Why are we doing that: Key mission is the acceleration of digital transformation in Europe in sovereign manner
- Different level of autonomy: hardware, legal aspects, technical autonomy -> (data: software + data set + license + hyper parameter of the training model you train = anything you can store in form of bits on a device)

- Trust Framework: enable and measure these levels of autonomy
-> The old process is: going to the qualification schema of a company and creating a document
-> This can be very complex and takes a lot of time and human resources (up to 2 years) -> Value is not good because you have this document only for hanging on the wall
-> Transformation: Europe can be seen as a highly fragmented market -> but we have in Europe the expertise and know-how to build complex advanced scenarios 
-> these „weakness of market fragmentation“ can be transformed to a strength, where we can build more complex services out of them -> this is what we call Service Compositions

- Service Composition: is the heart of the Trust Framework
= enables the creation of more complex composite services from atomic and elementary services as well as complex services 
-> ensure independence from hosting platforms and providers
-> which enables certain levels of freedom regarding to the hosting (to not have the lock-in or lock-out effects)
-> Trust Framework: points that are just covered from the architecture -> participants, providers, resources 
-> Very important is who signs it: Because if you start to identify who signs it, you can start to build the trust chain 
-> Differentes between the Trust Framework and the labeling: In the Trust Framework we have a set of claims, key value paira, keys
-> Please note: The Compliance Framework = former name of the Trust Framework

-> Example of 2nd order logic rule: All data sets must have a location; All Service Offering must have the provider’s legal country of jurisdiction
-> Example of  1st order logic rule: Data set must be located in EU; Services must be non-subject/ immune to non-EU laws
-> Trust is a non-binary statement (boolean) -> needs to be measured, for the services you can provide more detail for the description 
-> The draft of the Trust Index includes different levels of transparency -> that gives you in return the Trust Index 
-> With this Trust Index you can measure if you’re more or less trustworthy

Trust Framework summary: 
- Doesn’t take decision for the user- 2nd order rule
- Enforces Transparency
- Enables Portability - service compositions
- Enables Traceability - consent aggregation and policies attenuation
- (More in the slides)
- If you enter the software stack -> you’re not immune
-> it is important to know where is you’re software stack coming from -> in and outside EU 

- Link: https://gaia-x.atlassian.net/browse/LAB-88
-> Include description and further explanation what the working group does
-> For every typ of entity, the list of attributes that is required -> the GDPR is not mandatory
-> To the question „Will Eclipse data space connector be Gaia-X compliant or not“ -> can be

- Everyone is invited to read the document and write self-description -> that would be a good outcome of the hackathon

Marius Feldmann: 
- Nearly all sessions at the hackathon apart from the Newcomer session will write self-descriptions
-> That’s why it’s very important to check details that Pierre sketched 


3. Status of Gaia-X Hackathon #3 preparation

Marius Feldmann:
- Modification of the overall plan: The event will be only as an online event (no on-site option) 
- no critical amount is interested in on-side

[Track Updates]

Track 1: Newcomer & General Track (Marius Feldmann, Josephine Seifert):

Marius Feldmann: 
- Two things planned so far: -> Overview of the self-descriptions by Anja Strunk -> And so far discussed process how to add optional attributes to the self-descriptions by Josephine Seifert -> Looking for somebody who can give a general overview of Gaia-X: Pierre Gronlier can provide a overview (session is on 28 March, 10 am)
- General focus:  self description and overall track, guidance how to work with self-description -> along the Trust Framework 
- Next week the schedule will be finalized 

Track 2: Service & Tool Support Track (Martin Pilka & Kai Meinke):

Kai Meinke: 
- Splitting the content: Sub-Stream 1 (SD-Content Creation) and Sub-Stream 2 (Trust Issuance and Validation of Credentials)
- First proposals and next week there will be added more content and improvement 
- Some content was moved to ACME track 
- Landing page for the hackathon #3 is updated: Some content is renamed and first links for e.g. repositories
- Link to the landing page: https://hackathon.minimal-gaia-x.eu/
- It’s a community repository: Everyone participating in the track can adding content -> Everybody is invited to provide merge requests via: https://github.com/deltaDAO/mvg-hackathon-overview/blob/main/content/resources.json

Pierre Gronlier: 
- Pierre can present the Kalq project in 4-5 sentences -> What is it about? When sharing data you need  to enforce policy and rights 
-> Intention: To access information about the usage rights -> use self-description both as the rule and data for computing 
-> Information about who is the data provider and where is it located
- Kalg project is developed here: https://gitlab.com/gaia-x/lab/kalg
- > Gaia-X / Gaia-X Lab / kalg

Martin Pilka: 
- Regarding to the Python bindings for SD tools -> Found some issues regards compatibility
- We have to make a decision which option we’re going to support 
-> Python binding will be maybe the walt.id because is has all the functionalities we need right now 
-> We will do it in a way that it does not depend on the walt.id -> there’s an interface 
-> Track #6 all use these bindings 

Amjad Ibrahim: --> Do you have a deadline for freezing the features? 
-> No harsh deadline. Please bring your proposal in 
-> Send it to the mailing list and everyone can discuss it -> Next week would be good to bring it in 

Track 3: GXFS Track (Emma Wehrwein):

Lauresha Memeti:
- Registration is open and everyone can register now: https://gaia-x.eu/news/events/gaia-x-hackathon-3
- There is no update because the meeting is today
- Update next week

Track 4: ACME Track (Kurt Garloff):

Gert Machtelinckx:
- The track of operation should move in here because it has the best overlap to the missions 
- Next Wednesday: event with Structura-X member -> What is still needed in order to get Gaia-X operationalized and how that can be solved 
-> Details about tooling involved in the hackathon 

Kai Meinke: 
- Bootstrappping and setting up the infrastructure is important 
- We need also a strong link to the Trust Framework, Self-Description and Structura-X for the level of transparency we want to achieve
- If different infrastructure provider do this, this will be a much needed improvement 

Pierre Gronlier: 
- Before we have the Trust Framework it was been tolerated to name gaps because there was no definition
-> But with this document the management team will be very careful with anything that is mentioned with the Gaia-X compliance 

Marius Feldmann:
-> It's more evaluation work and not defining the compliance
-> Goal is to evaluate where we are and where gaps are -> overview of the current state of discussion 
-> Where are we in this discussion -> transfer of knowledge on hands-on level -> It is very essential to show what the term compliance means -> with technical insides

Kai Meinke:
-> My goal would be to have Gaia-X Compliant Descriptions here and Credentials in place 
-> doubt if it is possible to get there in time 

Marius Feldmann: 
-  You’re right,  the idea for this track was that that will be also part of hackathon #4 and #5

Track 5: Eclipse Dataspace Connector Track (Stefan Ettl):

- They will give an talk next week -> There are currently a lot of discussions on compliance 
- Next week also overview of the plans 

Kai Meinke: 
- It is not good, that a project of this size is not presented in this weekly meeting and always someone else is talking for the project

Marius Feldmann: 
- Yes, that’s why they will present it next week -> starting point for a common understanding

Pierre Gronlier: 
- There is a difference between the Gaia-X Compliance and the GXFS 
-> Cannot claim any Gaia-X compliance by itself 


Track 6: Deployment / Minimal Viable Gaia-X Track (Kai Meinke, Matej Feder):

Kai Meinke: 
-> Comittment to work towards the Gaia-X self-Description for the whole architecture stack including service characteristics and working on the claims 
-> Sign verify and hold the claims and to include them in the Self-Descriptions
-> To summarize: Transparency is key and enabling the trust

Matej Feder: 
- currently focus on discussion of self-descriptions 
- Working on data descriptions of the proposals 

Marius Feldmann: 
- We would like to see a simple process -> For seeing the different steps and to point to a reference tool -> to collect the tools for the different tasks 

Kai Meinke: 
- Can be done until next week 

4. AOB

Kai Meinke: 
- When registration to the hackathon: Does not include the term „developer“ -> what term should we use? 

Marius Feldmann: 
- The terms for roles of participants will be changed
-> Lauresha Memeti will drop a mail 

- Next week summary: Overview of the tools and a draft of the overall agenda to the newcomer session



